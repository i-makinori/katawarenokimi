


pandoc -H head.tex paper.md -o paper.pdf \
       --metadata-file=metadata.yaml \
       --number-sections \
       --table-of-contents --toc-depth=2 \
       -N --highlight-style=tango \
       -V indent=true \
       --filter pandoc-include \
       --pdf-engine=lualatex


<<'TEX'
#pandoc outline.md details.md conclusion.md -o curriculum_vitae.pdf \
pandoc -H manuscript/head.tex composition.md -o curriculum_vitae.pdf \
       --metadata-file=metadata.yaml \
       --number-sections \
       --table-of-contents --toc-depth=2 \
       -N --highlight-style=tango \
       -V indent=true \
       --filter pandoc-include \
       --pdf-engine=lualatex

TEX
